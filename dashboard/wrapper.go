package dashboard

import (
	"net/http"

	log "github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

// TODO: need close notifier

type ContextHandler interface {
	ServeHTTPContext(context.Context, http.ResponseWriter, *http.Request) error
}

// ContextHandlerFunc wraps func(context.Context, ResponseWriter, *Request)
type ContextHandlerFunc func(context.Context, http.ResponseWriter, *http.Request) error

func (f ContextHandlerFunc) ServeHTTPContext(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	return f(ctx, w, req)
}

type ContextAdapter struct {
	ctx     context.Context
	handler ContextHandler
}

func (ca *ContextAdapter) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if err := ca.handler.ServeHTTPContext(ca.ctx, w, req); err != nil {
		log.WithFields(log.Fields{
			"event_type": "error",
			"method":     req.Method,
			"path":       req.URL.Path,
			"error":      err,
		}).Errorln("ServeHTTP error")
	}
}
